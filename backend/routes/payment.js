const express = require("express");
const router = express.Router();

const {
  processPayment,
  processSSlPayment,
  sendStripeApi,
  validate,
  initRefund,
  refundQuery,
  tnQryById,
  tnQryBySession,
} = require("../controllers/paymentController");

const { isAuthenticatedUser } = require("../middlewares/auth");

// router.route("/payment/process").post(isAuthenticatedUser, processPayment);
// router.route("/stripeapi").get(isAuthenticatedUser, sendStripeApi);

//sslcommerz init
router.route("/payment/process").get(processSSlPayment);
router.route("/validate").get(validate);
router.route("/initiate-refund").get(initRefund);
router.route("/refund-query").get(refundQuery);
router.route("/transaction-query-by-transaction-id").get(tnQryById);
router.route("/transaction-query-by-session-id").get(tnQryBySession);

module.exports = router;
