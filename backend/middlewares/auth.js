const ErrorHandler = require('../utils/errorHandler')
const catchAsyncErrors = require('./catchAsyncErrors')
const jwt = require('jsonwebtoken')
const User = require('../models/user')

exports.isAuthenticatedUser = catchAsyncErrors (async (req, res, next) => {
     const {token} = req.cookies

     console.log('This is token from cookies =>', req.cookies)

    if(!token){
        return next( new ErrorHandler ('Login first to access this resource', 401))
    }

    const decoded = jwt.verify(token, process.env.JWT_SECRET)
    req.user = await User.findById(decoded.id)

    next()
})

// Handing users role
exports.authorizeRole = (...roles) => {
    return (req, res, next) => {
        if(!roles.includes(req.user.role)){
            return next(
                new ErrorHandler(`Role (${req.user.role}) is not allowed to access this resourse`, 403)
            )
        }

        next()
    }
}