const express = require("express");
const app = express();
const cookieParser = require("cookie-parser");
const fileupload = require("express-fileupload");
const errorMiddleware = require("./middlewares/errors");
const bodyParser = require("body-parser");
const payment = require("./routes/payment");
const dotenv = require("dotenv");
const path = require("path");

dotenv.config({ path: "backend/config/config.env" });

const cors = require("cors");

app.use(cors());
app.use(express.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(fileupload());

const products = require("./routes/product");
const users = require("./routes/user");
const order = require("./routes/order");

app.use("/api/v1", products);
app.use("/api/v1", users);
app.use("/api/v1", order);
app.use("/api/v1", payment);

if (process.env.NODE_ENV === "PRODUCTION") {
  app.use(express.static(path.join(__dirname, "../frontend/build")));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "../frontend/build/index.html"));
  });
}

// middle ware to handle error
app.use(errorMiddleware);

module.exports = app;
