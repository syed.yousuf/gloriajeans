const mongoose = require("mongoose");

const connectDatabase = () => {
  mongoose
    .connect(process.env.DB_DIG, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      tls: true,
      tlsCAFile: "./ca-certificate.crt",
    })
    .then((con) => {
      console.log(
        `MongoDB Database Connected with Host: ${con.connection.host}`
      );
    });
};

// const connectDatabase = () => {
//     mongoose.connect(process.env.DB_URI, {
//         useNewUrlParser: true,
//         useUnifiedTopology: true
//     }).then(con => {
//         console.log(`MongoDB Database Connected with Host: ${con.connection.host}`)
//     })
// }

module.exports = connectDatabase;
