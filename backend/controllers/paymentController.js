const catchAsyncErrors = require("../middlewares/catchAsyncErrors");
const stripe = require("stripe")(process.env.STRIPE_PUBLISH_KEY);

// ssl start from here
const SSLCommerzPayment = require("sslcommerz-lts");
const store_id = "navco61bf01591cd4a";
const store_passwd = "navco61bf01591cd4a@ssl";
const is_live = false; //true for live, false for sandbox

exports.processPayment = catchAsyncErrors(async (req, res, next) => {
  const paymentIntent = await stripe.paymentIntents.create({
    amount: req.body.amount,
    currency: "usd",

    metaData: { integration_check: "accept_a_payment" },
  });

  res.status(200).json({
    success: true,
    client_Secret: paymentIntent.client_secret,
  });
});

// Send stripe API key = /api/v1/stripeapi
exports.sendStripeApi = catchAsyncErrors(async (req, res, next) => {
  res.status(200).json({
    stripeApiKey: process.env.STRIPE_API_KEY,
  });
});

exports.tnQryBySession = catchAsyncErrors(async (req, res, next) => {
  const data = {
    sessionkey: AKHLAKJS5456454,
  };
  const sslcz = new SSLCommerzPayment(store_id, store_passwd, is_live);
  sslcz.transactionQueryBySessionId(data).then((data) => {
    //process the response that got from sslcommerz
    //https://developer.sslcommerz.com/doc/v4/#by-session-id
  });
});

exports.tnQryById = catchAsyncErrors(async (req, res, next) => {
  const data = {
    tran_id: AKHLAKJS5456454,
  };
  const sslcz = new SSLCommerzPayment(store_id, store_passwd, is_live);
  sslcz.transactionQueryByTransactionId(data).then((data) => {
    //process the response that got from sslcommerz
    //https://developer.sslcommerz.com/doc/v4/#by-session-id
  });
});

exports.refundQuery = catchAsyncErrors(async (req, res, next) => {
  const data = {
    refund_ref_id: SL4561445410,
  };
  const sslcz = new SSLCommerzPayment(store_id, store_passwd, is_live);
  sslcz.refundQuery(data).then((data) => {
    //process the response that got from sslcommerz
    //https://developer.sslcommerz.com/doc/v4/#initiate-the-refund
  });
});

exports.initRefund = catchAsyncErrors(async (req, res, next) => {
  const data = {
    refund_amount: 10,
    refund_remarks: "",
    bank_tran_id: CB5464321445456456,
    refe_id: EASY5645415455,
  };
  const sslcz = new SSLCommerzPayment(store_id, store_passwd, is_live);
  sslcz.initiateRefund(data).then((data) => {
    //process the response that got from sslcommerz
    //https://developer.sslcommerz.com/doc/v4/#initiate-the-refund
  });
});

//sslcommerz validation
exports.validate = catchAsyncErrors(async (req, res, next) => {
  const data = {
    val_id: ADGAHHGDAKJ456454, //that you go from sslcommerz response
  };
  const sslcz = new SSLCommerzPayment(store_id, store_passwd, is_live);
  sslcz.validate(data).then((data) => {
    //process the response that got from sslcommerz
    // https://developer.sslcommerz.com/doc/v4/#order-validation-api
  });
});
const crypto = require("crypto");
exports.processSSlPayment = catchAsyncErrors(async (req, res, next) => {
  console.log("I am from payment ssl");

  const dutu = JSON.parse(req.query.paymentData);

  console.log(dutu);

  const t_id = crypto.randomBytes(16).toString("hex");
  console.log(t_id);
  const data = {
    total_amount: dutu.order.totalPrice,
    currency: "BDT",
    tran_id: t_id, // use unique tran_id for each api call
    success_url: "http://localhost:3000/success",
    fail_url: "http://localhost:3000/fail",
    cancel_url: "http://localhost:3000/cancel",
    ipn_url: "http://localhost:3000/ipn",
    shipping_method: "Courier",
    product_name: "Gloria Jeans Products",
    product_category: "Food",
    product_profile: "general",
    cus_name: dutu.order.user.name,
    cus_email: dutu.order.user.email,
    cus_add1: "No Info",
    cus_add2: "No Info",
    cus_city: "No Info",
    cus_state: "No Info",
    cus_postcode: "No Info",
    cus_country: "No Info",
    cus_phone: "No Info",
    cus_fax: "no info",
    ship_name: "shipping name will be place soon",
    ship_add1: dutu.order.shippingInfo.address,
    ship_add2: dutu.order.shippingInfo.city,
    ship_city: dutu.order.shippingInfo.city,
    ship_state: dutu.order.shippingInfo.city,
    ship_postcode: dutu.order.shippingInfo.postalCode,
    ship_country: dutu.order.shippingInfo.country,
  };
  const sslcz = new SSLCommerzPayment(store_id, store_passwd, is_live);

  // res.status(200).json({
  //   success: true,
  //   sslcz,
  // });

  sslcz.init(data).then((apiResponse) => {
    // Redirect the user to payment gateway
    let GatewayPageURL = apiResponse.GatewayPageURL;
    // res.redirect(GatewayPageURL);
    res.status(200).json({
      GatewayPageURL,
      apiResponse,
    });
    // console.log(apiResponse);
    // console.log("Redirecting to: ", GatewayPageURL);
  });
});
