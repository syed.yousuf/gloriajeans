import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProducts } from "../actions/productAction";
import SelectSearch from "./SelectSearch";
import { addItemToCart } from "../actions/cartAction";
import Product from "./Product";
import SelectSearchTwo from "./SelectSearchTwo";

const CategoryFilter = ({ match }) => {
  const dispatch = useDispatch();

  const { loading, products } = useSelector((state) => state.products);

  const [data, setData] = useState(products);
  const [qty, setQty] = useState(1);

  const categoryFunction = (cat) => {
    const newData = products.filter((pp) => {
      return pp.category === cat;
    });
    setData(newData);
  };

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  const addToCart = () => {
    dispatch(addItemToCart(match.params.id, qty));
  };
  return (
    <>
      {/* <SelectSearch /> */}
      <SelectSearchTwo />
      <div class="menus col-md-12">
        {/* <!-- Left Category Bar --> */}
        <div class="mainmenus col-md-4 d-flex justify-content-center">
          <div class="holder d-flex flex-row mt-2">
            <ul
              class="nav nav-tabs nav-tabs--vertical nav-tabs--left"
              role="navigation"
            >
              <li class="nav-item">
                <a
                  href="#bf"
                  class="nav-link active"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="bf"
                  onClick={() => categoryFunction("Breakfast")}
                >
                  - Breakfast
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#dh"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="dh"
                  onClick={() => categoryFunction("Drinks (Hot)")}
                >
                  - Drinks (Hot)
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#ap"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="ap"
                  onClick={() => categoryFunction("Appetizer")}
                >
                  - Appetizer
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#snb"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="snb"
                  onClick={() => categoryFunction("Sandwich & Burger")}
                >
                  - Sandwich & Burger
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#dc"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="dc"
                  onClick={() => categoryFunction("Drinks (Cold)")}
                >
                  - Drinks (Cold)
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#sns"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="sns"
                  onClick={() => categoryFunction("Salads & Sides")}
                >
                  - Salads & Sides
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#str"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="str"
                  onClick={() => categoryFunction("Starters")}
                >
                  - Starters
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#mnp"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="mnp"
                  onClick={() => categoryFunction("Mains & Pastas")}
                >
                  - Mains & Pastas
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#pzz"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="pzz"
                  onClick={() => categoryFunction("Pizzas")}
                >
                  - Pizzas
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="#cs"
                  class="nav-link"
                  data-toggle="tab"
                  role="tab"
                  aria-controls="cs"
                  onClick={() => categoryFunction("Chef’s Special")}
                >
                  - Chef’s Special
                </a>
              </li>
            </ul>
          </div>
        </div>
        {/* <!-- Right Menu Items --> */}
        <div class="items col-md-12">
          <div>
            <h1 class="category-title text-center">Breakfast</h1>

            {/* <!-- Part 1 --> */}
            <div class="text-center">
              {data.map((product) => (
                <Product key={product._id} product={product} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CategoryFilter;
