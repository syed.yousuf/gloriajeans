import React from "react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";

const Sidebar = () => {
  //assigning location variable
  const location = useLocation();

  //destructuring pathname from location
  const { pathname } = location;

  // console.log(pathname);

  //Javascript split method to get the name of the path in array
  const splitLocation = pathname.split("/");
  console.log(splitLocation);
  return (
    <div className="mt-5">
      <nav id="sidebar">
        <ul className="list-unstyled components">
          <li className={splitLocation[1] === "dashboard" ? "active" : ""}>
            <Link to="/dashboard">
              <i className="fa fa-window-restore"></i> Dashboard
            </Link>
          </li>

          <li className={splitLocation[2] === "products" ? "active" : ""}>
            <Link
              // to="#productSubmenu"
              // data-toggle="collapse"
              // aria-expanded="false"
              // className="dropdown-toggle"
              to="/admin/products"
            >
              <i className="fab fa-product-hunt"></i>All Products
            </Link>
          </li>
          {/* <a
              href="#productSubmnu"
              data-toggle="collaps"
              aria-expanded="false"
              className="dropdown-toggle"
            >
              <i className="fa fa-product-hunt"></i>Products
            </a> */}
          {/* <ul className="collapse list-unstyled" id="productSubmenu"> */}
          {/* <li>
              <Link to="/admin/products">
                <i className="fa fa-clipboard"></i> All
              </Link>
            </li> */}

          <li className={splitLocation[2] === "productnew" ? "active" : ""}>
            <Link to="/admin/productnew">
              <i className="fa fa-plus"></i> Create
            </Link>
          </li>
          {/* </ul> */}

          <li className={splitLocation[2] === "orders" ? "active" : ""}>
            <Link to="/admin/orders">
              <i className="fa fa-shopping-basket"></i> Orders
            </Link>
          </li>

          <li className={splitLocation[2] === "users" ? "active" : ""}>
            <Link to="/admin/users">
              <i className="fa fa-users"></i> Users
            </Link>
          </li>
          {/* <li>
            <Link to="/admin/reviews">
              <i className="fa fa-star"></i> Reviews
            </Link>
          </li> */}
        </ul>
      </nav>
    </div>
  );
};

export default Sidebar;
