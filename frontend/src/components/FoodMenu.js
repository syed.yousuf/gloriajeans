import React from "react";
import FoodContent from "./food/FoodContent";
import FoodHeader from "./food/FoodHeader";
import ForTest from "./food/ForTest";

const FoodMenu = () => {
  return (
    <>
      <div className="container">
        <div className="mymizformenu">
          {/* <FoodHeader /> */}
          <FoodContent />
          {/* <ForTest /> */}
        </div>
      </div>
    </>
  );
};

export default FoodMenu;
