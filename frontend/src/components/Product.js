import React, { useState } from "react";
import { useAlert } from "react-alert";
import { useDispatch } from "react-redux";
import { addItemToCart } from "../actions/cartAction";

const Product = ({ product, match }) => {
  const [qty, setQty] = useState(1);
  const alert = useAlert();
  const dispatch = useDispatch();

  const addToCart = () => {
    dispatch(addItemToCart(product._id, qty));
    alert.success("Item Added To Cart");
  };

  return (
    <>
      <div className="col-md-6">
        <div
          class="td-breakfast gjc tab-pane fade show active"
          id="bf"
          role="tabpanel"
          title="Breakfast"
        >
          <div class="tab-content">
            <a href="#" class="quickviewItem">
              <img src="/Assets/img/cp.png" alt="" />
              <h2>{product.name}</h2>
              <p>{product.description}</p>
            </a>
            <button onClick={addToCart}>Add+</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Product;
