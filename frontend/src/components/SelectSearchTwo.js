import React from "react";

const SelectSearchTwo = () => {
  return (
    <>
      <div className="container text-white search-box">
        <div className="row">
          <div className="col-sm-3">
            <div className="del-option">
              <h4>Delivery to</h4>
              <select name="deliveryPlace" id="deliveryPlace">
                <option value="gulshan1">Gulshan 1</option>
                <option value="gulshan2">Gulshan 2</option>
                <option value="badda">Badda</option>
              </select>
            </div>
          </div>
          <div className="col-sm">
            <div class="searchboxholder">
              {/* <!-- Search Icon --> */}
              <div class="input-group-prepend">
                <span class="form-control">
                  <i class="fa fa-search" aria-hidden="true"></i>
                </span>
              </div>
              {/* <!-- Search Input box --> */}
              <input
                class="form-control"
                id="search-box"
                type="text"
                name="search"
                placeholder="Search food.. "
                required="required"
              />
            </div>
          </div>
          <div className="col-sm-2">
            <div class="input-group-append">
              <select
                class="form-control"
                id="search-filter"
                name="search-filter"
              >
                <div class="icon"></div>
                <option value="" selected disabled>
                  Filter
                </option>
                <option value="breakfast">Breakfast</option>
                <option value="lunch">Lunch</option>
                <option value="dinner">Dinner</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SelectSearchTwo;
