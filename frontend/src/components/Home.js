import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProducts } from "../actions/productAction";
import Loader from "./layouts/Loader";
import MetaData from "./layouts/MetaData";
import { useAlert } from "react-alert";
import Pagination from "react-js-pagination";
import Service from "./layouts/home/Service";
import YourTime from "./layouts/home/YourTime";
import Menu from "./layouts/home/Menu";
import FindOutlet from "./layouts/home/FindOutlet";
import Promotion from "./layouts/home/Promotion";
import Environment from "./layouts/home/Environment";
import OutlsetSlider from "./layouts/home/OutlsetSlider";
import Carousels from "./layouts/home/Carousel";
import CarouselTwo from "./layouts/home/CarouselTwo";

// home
const Home = () => {
  const [currentPage, setCurrentPage] = useState(1);

  const alert = useAlert();
  const dispatch = useDispatch();
  const { loading, products, error, productsCount, resPerPage } = useSelector(
    (state) => state.products
  );

  useEffect(() => {
    if (error) {
      return alert.error(error);
    }
    dispatch(getProducts(currentPage));
  }, [dispatch, alert, error]);

  function setCurrentPageNo(pageNumber) {
    setCurrentPage(pageNumber);
  }
  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <>
          <div className="container">
            <MetaData title={"Home"} />
            <div className="hero-center">
              {/* <img src="/Assets/img/home/hero.png" /> */}
              {/* <Carousels /> */}
              <CarouselTwo />
            </div>

            <Menu />
            <YourTime />
            <Promotion />
            <Service />
          </div>
          <FindOutlet />
          <div className="container">
            <OutlsetSlider />
          </div>
        </>
      )}
    </>
  );
};

export default Home;
