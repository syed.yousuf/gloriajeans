import React from "react";

const SelectSearch = () => {
  return (
    <>
      <div class="lower cont container col-md-9">
        <div class="row luca col-md-12">
          {/* <!-- Delivery Location --> */}
          <div class="delilocholder col-md-4">
            <div class="deliveryLocation col-md-12">
              <h4>Delivery to</h4>
              <select name="deliveryPlace" id="deliveryPlace">
                <option value="gulshan1">Gulshan 1</option>
                <option value="gulshan2">Gulshan 2</option>
                <option value="badda">Badda</option>
              </select>
            </div>
          </div>
          {/* <!-- Searchbox --> */}
          <div class="searchholder col-md-8">
            <div class="searchfilter">
              <div class="row">
                <div class="input-group">
                  {/* <!-- Search Box --> */}
                  <div class="searchboxholder">
                    {/* <!-- Search Icon --> */}
                    <div class="input-group-prepend">
                      <span class="form-control">
                        <i class="fa fa-search" aria-hidden="true"></i>
                      </span>
                    </div>
                    {/* <!-- Search Input box --> */}
                    <input
                      class="form-control"
                      id="search-box"
                      type="text"
                      name="search"
                      placeholder="Search food.. "
                      required="required"
                    />
                  </div>
                  {/* <!-- Select Filter --> */}
                  <div class="input-group-append">
                    <select
                      class="form-control"
                      id="search-filter"
                      name="search-filter"
                    >
                      <div class="icon"></div>
                      <option value="" selected disabled>
                        Filter
                      </option>
                      <option value="breakfast">Breakfast</option>
                      <option value="lunch">Lunch</option>
                      <option value="dinner">Dinner</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SelectSearch;
