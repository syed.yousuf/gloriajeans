import React, { useState } from "react";

import MetaData from "../layouts/MetaData";

import { useAlert } from "react-alert";

import { useDispatch, useSelector } from "react-redux";
import { Fragment, useEffect } from "react";
import { createOrder, clearErrors } from "../../actions/orderAction";

import { Link, Redirect } from "react-router-dom";

// import {
//   useStripe,
//   useElements,
//   CardNumberElement,
//   CardExpiryElement,
//   CardCvcElement,
// } from "@stripe/react-stripe-js";

import axios from "axios";
import CheckoutSteps from "./CheckoutSteps";

const options = {
  style: {
    base: {
      fontSize: "16px",
    },
    invalid: {
      color: "#000",
    },
  },
};

const Payment = ({ history }) => {
  const alert = useAlert();
  // const stripe = useStripe();
  // const elements = useElements();
  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.user);
  // console.log(user);
  const { cartItems, shippingInfo } = useSelector((state) => state.cart);

  const { error } = useSelector((state) => state.newOrder);

  const orderInfo = JSON.parse(sessionStorage.getItem("orderInfo"));

  useEffect(() => {
    if (error) {
      alert.error(error);
      dispatch(clearErrors());
    }
  }, [dispatch, alert, error]);

  const order = {
    orderItems: cartItems,
    shippingInfo,
    user,
  };

  if (orderInfo) {
    order.itemsPrice = orderInfo.itemsPrice;
    order.shippingPrice = orderInfo.shippingPrice;
    order.taxPrice = orderInfo.taxPrice;
    order.totalPrice = orderInfo.totalPrice;
  }

  const paymentData = {
    order: order,
  };
  // const paymentData = "120";

  const submitHandler = async (e) => {
    e.preventDefault();

    document.querySelector("#pay_btn").disabled = true;

    let res;

    try {
      const config = {
        // method: "POST",
        headers: { "Content-Type": "application/json" },
        params: {
          paymentData,
        },
      };

      await axios
        .get("/api/v1/payment/process", config)
        .then((resp) => {
          // console.log(resp);
          window.location.replace(resp.data.GatewayPageURL);
          dispatch(createOrder(order));
        })
        .catch((err) => {
          console.log(err.response);
        });

      // console.log(res.data.GatewayPageURL);

      // const clientSecret = res.data.client_Secret;

      // if (res) {
      // <Link to={{ pathname: res.data.GatewayPageURL }} target="_blank" />;
      // return <Redirect to={res.data.GatewayPageURL} />;

      //   const link = res.data.GatewayPageURL;
      //   console.log(res.data);
      //   const resLink = await window.open(link);

      //   console.log(resLink);
      // }

      // const result = await stripe.confirmCardPayment(clientSecret, {
      //   payment_method: {
      //     card: elements.getElement(CardNumberElement),
      //     billing_details: {
      //       name: user.name,
      //       email: user.email,
      //     },
      //   },
      // });

      // if (result.error) {
      //   alert.error(result.error.message);
      //   document.querySelector("#pay_btn").disabled = false;
      // } else {
      //   if (result.paymentIntent.status === "succeeded") {
      //     // todo
      //     order.paymentInfo = {
      //       id: result.paymentIntent.id,
      //       status: result.paymentIntent.status,
      //     };

      //     dispatch(createOrder(order));
      //     history.push("/success");
      //   } else {
      //     alert.error("There is some issues while proccessing the payment");
      //   }
      // }
    } catch (error) {
      // document.querySelector("#pay_btn").disabled = false;
      // alert.error(error.response.data.message);
      // console.log(error.response.data);
    }
  };

  return (
    <Fragment>
      <MetaData title={"Payment Informaiton"} />

      {/* <CheckoutSteps shipping confirmOrder payment /> */}

      <div className="row wrapper mymiz my-5">
        <div className="col-10 col-lg-5">
          <form className="shadow-lg" onSubmit={submitHandler}>
            <h1 className="mb-4">Card Informaiton</h1>
            {/* <div className="form-group">
              <label htmlFor="card_num_field">Card Number</label>
              <CardNumberElement
                type="text"
                className="form-control"
                options={options}
              />
            </div>

            <div className="form-group">
              <label htmlFor="card_exp_field">Card Expiry</label>
              <CardExpiryElement
                type="text"
                id="card_exp_field"
                className="form-control"
                options={options}
              />
            </div>

            <div className="form-group">
              <label htmlFor="card_cvc_field">Card CVC</label>
              <CardCvcElement
                type="text"
                id="card_cvc_field"
                className="form-control"
                options={options}
              />
            </div> */}

            <button id="pay_btn" type="submit" className="btn btn-block py-3">
              Pay {` - ${orderInfo && orderInfo.totalPrice}`}
            </button>
          </form>
        </div>
      </div>
    </Fragment>
  );
};

export default Payment;
