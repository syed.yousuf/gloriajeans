import React from "react";
import { useAlert } from "react-alert";
import { useDispatch, useSelector } from "react-redux";
import { Route, Link } from "react-router-dom";
import { logout } from "../../actions/userAction";
const HeaderTwo = () => {
  const alert = useAlert();
  const dispatch = useDispatch();

  const { cartItems } = useSelector((state) => state.cart);

  const { user, loading } = useSelector((state) => state.user);
  const logoutHanlder = () => {
    dispatch(logout());
    alert.success("Logged out successfully!");
  };
  return (
    <>
      <div className="text-white text-right">
        <div className="top-nav ml-100">
          <div className="social-head">
            <a
              href="https://www.facebook.com/gloriajeanscoffeesbangladesh/"
              target="_blank"
            >
              <i class="fab fa-facebook-f"></i>
            </a>
            <a
              href="https://instagram.com/gloriajeanscoffeesbd"
              target="_blank"
            >
              <i class="fab fa-instagram"></i>
            </a>
            <a
              href="https://www.youtube.com/channel/UC1qR4RXd60ix5zOaKOKSd1g"
              target="_blank"
            >
              <i class="fab fa-youtube"></i>
            </a>
            <a href="#">
              <i class="fab fa-twitter"></i>
            </a>
          </div>
        </div>
      </div>
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container text-white">
          <Link to="/">
            <img
              src="/Assets/img/home/gjc_officialgjc.svg"
              alt=""
              srcset=""
              class="main-logo"
              width="90"
              height="90"
            />
          </Link>
          <button
            type="button"
            class="navbar-toggler"
            data-bs-toggle="collapse"
            data-bs-target="#navbarCollapse"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div
            class="collapse navbar-collapse justify-content-between"
            id="navbarCollapse"
          >
            <div class="navbar-nav">
              <Link to="/" class="nav-item nav-link">
                Home
              </Link>

              <div class="nav-item dropdown">
                <Link
                  to="#"
                  class="nav-link dropdown-toggle"
                  data-bs-toggle="dropdown"
                >
                  Order Now
                </Link>
                <div class="dropdown-menu">
                  <Link to="/menupage" class="dropdown-item">
                    Food
                  </Link>
                  <Link to="#" class="dropdown-item">
                    Drink
                  </Link>
                  <Link to="#" class="dropdown-item">
                    Merchandise
                  </Link>
                </div>
              </div>
              <Link to="#" class="nav-item nav-link">
                About Us
              </Link>
              <Link to="#" class="nav-item nav-link">
                Loyalty Program
              </Link>
            </div>
            <div className="col-12 col-md-5 mt-4 mt-md-0 text-center">
              <a
                className="text-white nav-location"
                href={"http://localhost:3000/#location"}
              >
                <img src="/Assets/img/home/location.svg" width="30" alt="" />
                Find a store
              </a>
              <Link to="/cart" style={{ textDecoration: "none" }}>
                <span id="cart" className="ml-3">
                  <img src="/Assets/img/cartButton.svg" alt="" srcset="" />
                </span>
                <span id="cart_count" className="ml-1">
                  {cartItems.length}
                </span>
              </Link>
              {user ? (
                <div className="ml-4 dropdown d-inline">
                  <Link
                    to="#!"
                    className="btn dropdown-toggle text-white mr-4"
                    type="button"
                    id="dropDownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <figure className="avatar avatar-nav">
                      <img
                        src={user.avatar && user.avatar.url}
                        alt={user && user.name}
                        className="rounded-circle"
                      />
                    </figure>
                    <span>{user && user.name}</span>
                  </Link>

                  <div
                    className="dropdown-menu"
                    aria-labelledby="dropDownMenuButton"
                  >
                    {user && user.role === "admin" && (
                      <Link className="dropdown-item" to="/dashboard">
                        Dashboard
                      </Link>
                    )}
                    <Link className="dropdown-item" to="/orders/me">
                      Orders
                    </Link>
                    <Link className="dropdown-item" to="/me">
                      Profile
                    </Link>
                    <Link
                      className="dropdown-item text-danger"
                      to="/"
                      onClick={logoutHanlder}
                    >
                      Logout
                    </Link>
                  </div>
                </div>
              ) : (
                !loading && (
                  <>
                    <Link to="/login" className="ml-4 login_btn">
                      Login
                    </Link>
                    <Link to="/register" className="ml-2 login_btn">
                      Register
                    </Link>
                  </>
                )
              )}
            </div>
            {/* <form class="d-flex">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" />
                <button type="button" class="btn btn-secondary">
                  <i class="bi-search"></i>
                </button>
              </div>
            </form>
            <div class="navbar-nav">
              <a href="#" class="nav-item nav-link">
                Login
              </a>
            </div> */}
          </div>
        </div>
      </nav>
    </>
  );
};

export default HeaderTwo;
