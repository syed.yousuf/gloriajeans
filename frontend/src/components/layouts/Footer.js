import React from "react";

const Footer = () => {
  return (
    <>
      {/* <!-- Footer Starts --> */}
      <footer class="gjc-footer">
        <div class="footerHolder">
          {/* <!-- Footer Left --> */}
          <div class="footerLeft">
            <div class="footerHeadings">
              <h1>Contact Info</h1>
            </div>
            <div class="contactList">
              <li>
                <a href="#">
                  <img src="/Assets/img/support.png" alt="" srcset="" />
                  For query: (+880) 1929 333 999
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="/Assets/img/clock.png" alt="" srcset="" />
                  Call Time: 9.00 AM to 9.00 PM
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="/Assets/img/location.png" alt="" srcset="" />
                  Corporate Address:
                  <br />
                  House 2B, Block NE(B), Road 71
                  <br />
                  Gulshan North Avenue, Dhaka-1212
                </a>
              </li>
            </div>
          </div>

          {/* <!-- Footer Middle --> */}
          <div class="footerMiddle">
            <div class="footerHeadings">
              <h1>My Account</h1>
            </div>
            <div class="accountList">
              <li>
                <a href="#">Employee Login</a>
              </li>
              <li>
                <a href="#">Career</a>
              </li>
              <li>
                <a href="#">Contact Us</a>
              </li>
            </div>
          </div>

          {/* <!-- Footer Outlets --> */}
          <div class="footeroutlets">
            <div class="footerHeadings">
              <h1>Outlets</h1>
            </div>
            <div class="outletList">
              <li>
                <a href="#">
                  Badda Outlet :<br />
                  Rangs RL Square. Pragati Sharani. Dhaka-1212
                </a>
              </li>
              <li>
                <a href="#">
                  Gulshan 1 Outlet :<br />
                  S.W (a), House 35, Gulshan Avenue, Dhaka-1212
                </a>
              </li>
              <li>
                <a href="#">
                  Gulshan 2 Outlet :<br />
                  House 2B, Block NE (B), Road 71, <br />
                  Gulshan North Avenue, Dhaka 1212
                </a>
              </li>
              <li>
                <a href="#">
                  Dhanmondi Outlet :<br />
                  67 (New), 767 (Old), GH Heights (1st Floor),
                  <br />
                  Satmasjid Road, Dhaka-1209
                </a>
              </li>
            </div>
          </div>

          {/* <!-- Footer Right --> */}
          <div class="footerRight">
            <div class="footerHeadings">
              <h1>Follow Us</h1>
            </div>
            <div class="socialIcons">
              <a href="https://www.facebook.com/gloriajeanscoffeesbangladesh">
                <i class="fab fa-facebook-f"></i>
              </a>
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
              <a href="https://instagram.com/gloriajeanscoffeesbd">
                <i class="fab fa-instagram"></i>
              </a>
              <a href="https://www.youtube.com/channel/UC1qR4RXd60ix5zOaKOKSd1g">
                <i class="fab fa-youtube"></i>
              </a>
            </div>
          </div>
        </div>

        {/* <!-- Copyright --> */}
        <div class="copyrighter">
          <h4>
            <i class="far fa-copyright"> Gloria Jean’s Coffees Bangladesh.</i>
            <h4>All rights reserved.</h4>
          </h4>
        </div>
      </footer>
      {/* <!-- Footer Ends --> */}
    </>
  );
};

export default Footer;
