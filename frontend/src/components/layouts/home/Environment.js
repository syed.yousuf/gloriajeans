import React, { useState } from "react";

const Environment = (props) => {
  return (
    <>
      <div className="env text-white">
        <div className="row">
          <div className="col-md-6">
            <img src={props.img} />
          </div>
          <div className="col-md-6">
            <img src="/Assets/img/home/environment.png" />
            <h2 className="rest-head">
              Visit <span className="your-time">Our Outlets</span>
            </h2>
            <h3>{props.sl}</h3>
            <p>{props.address}</p>
            <button className="btn btn-ser">
              <a href={props.loc} target="_blank">
                Go to Map
              </a>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Environment;
