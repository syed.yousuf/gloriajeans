import React from "react";

const YourTime = () => {
  return (
    <>
      <div className="container text-center text-white">
        <img src="/Assets/img/home/facilities.png" />
        <h2 className="yourtime-head">
          Only Enjoy <span className="your-time">Your Time</span>
        </h2>

        <div class="row">
          <div class="col-md-2 col-sm-6 col-xs-6">
            <div className="mt-5 yt-bt-text">
              <img src="/Assets/img/home/icons/outdoor-setting.png" />
              <h4>
                Outdoor <br /> Setting
              </h4>
            </div>
          </div>
          <div class="col-md-2 col-sm-6 col-xs-6">
            <div className="mt-5 yt-bt-text">
              <img src="/Assets/img/home/icons/meeting-room.png" />
              <h4>
                Meeting <br /> Room
              </h4>
            </div>
          </div>
          <div class="col-md-2 col-sm-6 col-xs-6">
            <div className="mt-5 yt-bt-text">
              <img src="/Assets/img/home/icons/kids-zone.png" />
              <h4>
                Kid's <br /> Zone
              </h4>
            </div>
          </div>
          <div class="col-md-2 col-sm-6 col-xs-6">
            <div className="mt-5 yt-bt-text">
              <img src="/Assets/img/home/icons/night-hangout.png" />
              <h4>
                Late Night <br /> Hangout
              </h4>
            </div>
          </div>
          <div class="col-md-2 col-sm-6 col-xs-6">
            <div className="mt-5 yt-bt-text">
              <img src="/Assets/img/home/icons/car-facilities.png" />
              <h4>
                Parking <br /> Facilities
              </h4>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default YourTime;
