import React from "react";
import { Link } from "react-router-dom";

const Menu = () => {
  return (
    <>
      <div className="container home-menu text-white">
        <img src="/Assets/img/home/menu.png" />
        <h2 className="yourtime-head">
          <span className="your-time">Our Coffee</span> will always Make <br />{" "}
          You Tempted
        </h2>

        <div className="row menu-content">
          <div className="col-sm">
            <img src="/Assets/img/home/cup-with-shadow.png" />
            <div className="menu-text">
              <h3>White Choklate Mocha</h3>
              <p>Gloria Jeans Coffees Bangladesh brought</p>
              {/* <button className="btn btn-menu">Order Now</button> */}
            </div>
          </div>

          <div className="col-sm">
            <img src="/Assets/img/home/black-cup-with-shadow.png" />
            <div className="menu-text">
              <h3>White Choklate Mocha</h3>
              <p>Gloria Jeans Coffees Bangladesh brought</p>
              {/* <button className="btn btn-menu">Order Now</button> */}
            </div>
          </div>

          <div className="col-sm">
            <img src="/Assets/img/home/cup-with-shadow.png" />
            <div className="menu-text">
              <h3>White Choklate Mocha</h3>
              <p>Gloria Jeans Coffees Bangladesh brought</p>
              {/* <button className="btn btn-menu">Order Now</button> */}
            </div>
          </div>
        </div>

        <button className="btn btn-ser">
          <Link to="menupage">View Full Menu</Link>
        </button>
      </div>
    </>
  );
};

export default Menu;
