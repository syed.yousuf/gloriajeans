import React, { useState } from "react";

import ReactSlidy from "react-slidy";
import "react-slidy/lib/styles.css";
import Environment from "./Environment";

const SLIDES = [
  ["./Assets/img/home/gulshan-1.jpg", "Gulshan 1"],
  ["./Assets/img/home/gulshan-2.jpg", "Gulshan 2"],
  ["./Assets/img/home/dhanmondi.jpg", "Dhanmondi"],
  ["./Assets/img/home/badda.jpg", "Badda"],
];

const GoogleMap = [
  "https://g.page/Gloria_Jeans_Coffees_Gulshan_1?share",
  "https://g.page/Gloria_Jeans_Coffees_Gulshan_2?share",
  "https://g.page/gloria-jean-s-coffees-dhanmondi?share",
  "https://goo.gl/maps/Yr11gtyXhyshDfn47",
];

const createStyles = (isActive) => ({
  background: "transparent",
  border: 0,
  color: isActive ? "#ff8600" : "#ccc",
  cursor: "pointer",
  fontSize: isActive ? "42px" : "40px",
  outline: "none",
});

const mystyle = (snn) => ({
  color: snn ? "#ff8600" : "white",
  fontSize: "25px",
});

const OutlsetSlider = () => {
  const [actualSlide, setActualSlide] = useState(0);
  const [sl, setSl] = useState("Gulshan 1");
  const [location, setLocation] = useState(
    "https://g.page/Gloria_Jeans_Coffees_Gulshan_1?share"
  );
  const [address, setAddress] = useState(
    "S.W (a), House 35, Gulshan Avenue, Dhaka-1212"
  );
  const [img, setImg] = useState("/Assets/img/home/gulshan-1.jpg");
  const [active, setActive] = useState(false);

  const updateSlide = ({ currentSlide }) => {
    setActualSlide(currentSlide);
    // console.log(currentSlide);
  };

  const sliderChange = (val) => {
    setSl(val);
    if (val === "Gulshan 1") {
      setAddress("S.W (a), House 35, Gulshan Avenue, Dhaka-1212");
      setImg("/Assets/img/home/gulshan-1.jpg");
      setLocation("https://g.page/Gloria_Jeans_Coffees_Gulshan_1?share");
    }
    if (val === "Gulshan 2") {
      setAddress(
        "House 2B, Block NE (B), Road 71, Gulshan North Avenue, Dhaka 1212"
      );
      setImg("/Assets/img/home/gulshan-2.jpg");
      setLocation("https://g.page/Gloria_Jeans_Coffees_Gulshan_2?share");
    }
    if (val === "Dhanmondi") {
      setAddress(
        "67 (New), 767 (Old), GH Heights (1st Floor), Satmasjid Road, Dhaka-1209"
      );
      setImg("/Assets/img/home/dhanmondi.jpg");
      setLocation("https://g.page/gloria-jean-s-coffees-dhanmondi?share");
    }
    if (val === "Badda") {
      setAddress("Rangs RL Square. Pragati Sharani. Dhaka-1212");
      setImg("/Assets/img/home/badda.jpg");
      setLocation("https://goo.gl/maps/Yr11gtyXhyshDfn47");
    }
  };

  return (
    <>
      <>
        <Environment
          sl={sl}
          address={address}
          img={img}
          actualSlide={actualSlide}
          loc={location}
        />
      </>
      <ReactSlidy
        doAfterSlide={updateSlide}
        slide={actualSlide}
        numOfSlides={4}
      >
        {SLIDES.map((src, index) => (
          <>
            <div className="slide-content">
              <img alt={src[1]} key={src[0]} src={src[0]} />
              <h4
                style={mystyle(index === actualSlide)}
                onClick={() => sliderChange(src[1])}
              >
                {src[1]}
              </h4>
            </div>
          </>
        ))}
      </ReactSlidy>
      <div className="Dots">
        {SLIDES.map((_, index) => {
          return (
            <button
              key={index}
              style={createStyles(index === actualSlide)}
              onClick={() => updateSlide({ currentSlide: index })}
            >
              &bull;
            </button>
          );
        })}
      </div>
    </>
  );
};

export default OutlsetSlider;
