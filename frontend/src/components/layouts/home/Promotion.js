import React from "react";

const Promotion = () => {
  return (
    <>
      <div className="promotion text-white text-center">
        <img src="/Assets/img/home/promotions.png" />
        <h2>
          Get The <span className="your-time">Best Deal</span>
        </h2>
        <div className="row">
          <div className="col-md-6">
            <img src="/Assets/img/home/coffee50.svg" width="570" />
          </div>
          <div className="col-md-6">
            <div className="fl">
              <img src="/Assets/img/home/sixtyoff.svg" />
              <img src="/Assets/img/home/twienty25off.svg" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Promotion;
