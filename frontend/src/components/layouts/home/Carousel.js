import React from "react";
const Carousel = require("react-responsive-carousel").Carousel;

const Carousels = () => {
  return (
    <>
      <Carousel
      // showArrows={true}
      // onChange={onChange}
      // onClickItem={onClickItem}
      // onClickThumb={onClickThumb}
      >
        <div>
          <img src="Assets/img/home/carousel/hero.png" />
          <p className="legend">Legend 1</p>
        </div>
        <div>
          <img src="Assets/img/home/carousel/hero.png" />
          <p className="legend">Legend 2</p>
        </div>
        <div>
          <img src="Assets/img/home/carousel/hero.png" />
          <p className="legend">Legend 3</p>
        </div>
      </Carousel>
    </>
  );
};

export default Carousels;
