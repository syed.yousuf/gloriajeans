import React from "react";

const FindOutlet = () => {
  return (
    <>
      <div className="find-outlet text-center text-white" id="location">
        <h4 className="mb-3">Find An Outlet Near You</h4>
        <button className="btn btn-ser">Search</button>
      </div>
    </>
  );
};

export default FindOutlet;
