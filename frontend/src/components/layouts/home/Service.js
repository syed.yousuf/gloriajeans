import React from "react";

const Service = () => {
  return (
    <>
      <div className="services">
        <h2 className="text-center">Our Services</h2>
        <div class="container servics-in">
          <div class="row text-white text-center">
            <div class="col-sm ser-back">
              <img src="/Assets/img/home/coffeecup.png" width="210" />
              <div className="ser-inner-text">
                <h3>Pick-up</h3>
                <p>
                  Gloria Jean’s Coffees Bangladesh brought & developed by Navana
                  Foods Ltd
                </p>
                <button className="btn btn-ser">Order Now</button>
              </div>
            </div>
            <div class="col-sm ser-back">
              <img src="/Assets/img/home/coffeemag.png" width="210" />
              <div className="ser-inner-text">
                <h3>Delivery</h3>
                <p>
                  Gloria Jean’s Coffees Bangladesh brought & developed by Navana
                  Foods Ltd
                </p>
                <button className="btn btn-ser">Order Now</button>
              </div>
            </div>
            <div class="col-sm ser-back">
              <img src="/Assets/img/home/coffeemag.png" width="210" />
              <div className="ser-inner-text">
                <h3>Dine-in</h3>
                <p>
                  Gloria Jean’s Coffees Bangladesh brought & developed by Navana
                  Foods Ltd
                </p>
                {/* <button className="btn btn-ser">Order Now</button> */}
              </div>
            </div>
            <div class="col-sm ser-back">
              <img src="/Assets/img/home/coffeemag.png" width="210" />
              <div className="ser-inner-text">
                <h3>Takeaway</h3>
                <p>
                  Gloria Jean’s Coffees Bangladesh brought & developed by Navana
                  Foods Ltd
                </p>
                {/* <button className="btn btn-ser">Order Now</button> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Service;
