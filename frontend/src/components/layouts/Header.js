import React from "react";
import { useAlert } from "react-alert";
import { useDispatch, useSelector } from "react-redux";
import { Route, Link } from "react-router-dom";
import { logout } from "../../actions/userAction";

const Header = () => {
  const alert = useAlert();
  const dispatch = useDispatch();

  const { cartItems } = useSelector((state) => state.cart);

  const { user, loading } = useSelector((state) => state.user);
  const logoutHanlder = () => {
    dispatch(logout());
    alert.success("Logged out successfully!");
  };
  return (
    <>
      {/* <!-- Navbar start ccsssssss --> */}
      <div class="main-navbar font-gothic col-md-10 container">
        <nav class="cont">
          <div class="logo col-md-2">
            <Link to="/">
              <img
                src="/Assets/img/gjcLogoOfficial.png"
                alt=""
                srcset=""
                class="main-logo"
              />
            </Link>
          </div>
          {/* <!-- Link List of menus --> */}
          <ul class="nav-links col-md-6">
            <li>
              <a href="#">Home</a>
            </li>
            <li>
              <a href="/menu.html">Order Now</a>
              <ul>
                <li>
                  <a href="#">Food</a>
                </li>
                <li>
                  <Link to="/menupage">Drink</Link>
                </li>
                <li>
                  <a href="#">Merchandise</a>
                </li>
                {/* <li>
                  <a href="#">
                    <Link to="/menupage">Menu Page</Link>
                  </a>
                </li> */}
              </ul>
            </li>
            <li>
              <a href="#">About Us</a>
            </li>
            <li>
              <a href="#">Loyalty Program</a>
            </li>
          </ul>
          {/* <!-- Cart Button --> */}
          {/* <div class="cart"> */}
          {/* <a href="#"> */}
          {/* <!-- <img src="/Assets/img/cartButton.svg" alt="" srcset=""> --> */}
          {/* <i class="cartWhite">
                <img src="/Assets/img/cartButton.svg" alt="" srcset="" />
              </i>
              <i class="cartOrange">
                <img src="/Assets/img/cartButtonOrange.svg" alt="" srcset="" />
              </i> */}
          {/* </a> */}
          {/* </div> */}

          {/* section by mizan ss */}
          <div className="col-12 col-md-5 mt-4 mt-md-0 text-center">
            <Link to="/cart" style={{ textDecoration: "none" }}>
              <span id="cart" className="ml-3">
                <img src="/Assets/img/cartButton.svg" alt="" srcset="" />
              </span>
              <span id="cart_count" className="ml-1">
                {cartItems.length}
              </span>
            </Link>

            {user ? (
              <div className="ml-4 dropdown d-inline">
                <Link
                  to="#!"
                  className="btn dropdown-toggle text-white mr-4"
                  type="button"
                  id="dropDownMenuButton"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <figure className="avatar avatar-nav">
                    <img
                      src={user.avatar && user.avatar.url}
                      alt={user && user.name}
                      className="rounded-circle"
                    />
                  </figure>
                  <span>{user && user.name}</span>
                </Link>

                <div
                  className="dropdown-menu"
                  aria-labelledby="dropDownMenuButton"
                >
                  {user && user.role === "admin" && (
                    <Link className="dropdown-item" to="/dashboard">
                      Dashboard
                    </Link>
                  )}
                  <Link className="dropdown-item" to="/orders/me">
                    Orders
                  </Link>
                  <Link className="dropdown-item" to="/me">
                    Profile
                  </Link>
                  <Link
                    className="dropdown-item text-danger"
                    to="/"
                    onClick={logoutHanlder}
                  >
                    Logout
                  </Link>
                </div>
              </div>
            ) : (
              !loading && (
                <>
                  <Link to="/login" className="ml-4 login_btn">
                    Login
                  </Link>
                  <Link to="/register" className="ml-4 login_btn">
                    Register
                  </Link>
                </>
              )
            )}
          </div>

          {/* <!-- Burger Button --> */}
          <div class="burger">
            <div class="plates">
              <div
                class="plate plate5"
                onclick="this.classList.toggle('active')"
              >
                <svg
                  class="burger"
                  version="1.1"
                  height="100"
                  width="100"
                  viewBox="0 0 100 100"
                >
                  <path class="line line1" d="M 30,35 H 70 " />
                  <path class="line line2" d="M 50,50 H 30 L 34,32" />
                  <path class="line line3" d="M 50,50 H 70 L 66,68" />
                  <path class="line line4" d="M 30,65 H 70 " />
                </svg>
                <svg
                  class="x"
                  version="1.1"
                  height="100"
                  width="100"
                  viewBox="0 0 100 100"
                >
                  <path class="line" d="M 34,32 L 66,68" />
                  <path class="line" d="M 66,32 L 34,68" />
                </svg>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </>
  );
};

export default Header;
