import React from "react";

const ProductData = () => {
  return (
    <>
      <div class="items col-md-8">
        <div class="table-responsive">
          <h1 class="text-center">Breakfast</h1>
          <table class="table responsive" style="min-width:800px;">
            <tbody>
              <tr class="listitem text-center">
                <td class="td-breakfast gjc" title="Breakfast">
                  <img src="/Assets/img/cp.png" alt="" />
                  <h1>Chicken Picasso 1</h1>
                  <p>Gloria Jean’s Coffees Bangladesh brought</p>
                  <button>Add+</button>
                </td>
                <td class="td-lunch gjc" title="Lunch">
                  <img src="/Assets/img/cp.png" alt="" />
                  <h1>Mutton Picasso 2</h1>
                  <p>Gloria Jean’s Coffees Bangladesh brought</p>
                  <button>Add+</button>
                </td>
              </tr>
              <tr class="listitem text-center">
                <td class="td-dinner gjc" title="dinner">
                  <img src="/Assets/img/cp.png" alt="" />
                  <h1>Beef Picasso 3</h1>
                  <p>Gloria Jean’s Coffees Bangladesh brought</p>
                  <button>Add+</button>
                </td>
                <td class="td-lunch gjc" title="lunch">
                  <img src="/Assets/img/cp.png" alt="" />
                  <h1>Mutton Steak 4</h1>
                  <p>Gloria Jean’s Coffees Bangladesh brought</p>
                  <button>Add+</button>
                </td>
              </tr>
              <tr class="listitem text-center">
                <td class="td-breakfast gjc" title="Breakfast">
                  <img src="/Assets/img/cp.png" alt="" />
                  <h1>Chicken Steak 5</h1>
                  <p>Gloria Jean’s Coffees Bangladesh brought</p>
                  <button>Add+</button>
                </td>
                <td class="td-dinner gjc" title="Dinner">
                  <img src="/Assets/img/cp.png" alt="" />
                  <h1>Beef Steak 6</h1>
                  <p>Gloria Jean’s Coffees Bangladesh brought</p>
                  <button>Add+</button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};
export default ProductData;
