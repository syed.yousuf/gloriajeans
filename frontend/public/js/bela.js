function changeColor(event) {
  var red = document.getElementById(red);
  var green = document.getElementById(green);
  var blue = document.getElementById(blue);

  if (event.target.value == red) {
    red.style.color = "red";
  } else if (event.target.value == green) {
    green.style.color = "green";
  } else if (event.target.value == blue) {
    blue.style.color = "blue";
  } else {
    alert("There was an error!");
  }};